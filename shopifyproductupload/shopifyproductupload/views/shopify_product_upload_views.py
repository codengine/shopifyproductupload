from django.http import HttpResponse
from django.views import View
from django.views.generic import ListView
from shopifyproductupload.models.product import Product


class ShopifyProductUploadView(ListView):
    template_name='index.html'
    model=Product