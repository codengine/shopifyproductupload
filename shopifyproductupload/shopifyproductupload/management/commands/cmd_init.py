from django.core.management.base import BaseCommand

from shopio.download_resources import Downloader

from shopio.browser import Browser


class Command(BaseCommand):
    def handle(self, *args, **options):
        url = 'https://www.pinterest.ca/pin/57772807705661838/'
        browser = Browser()
        browser.OpenURL(url)
        page = browser.GetPage()
        # print(page)
        # return
        downloader = Downloader()
        download_path = '/media/codenginebd/MyWorksandStuffs3/Projects/shopifyproductupload/images'
        downloader.download_all_images(page, download_path)


