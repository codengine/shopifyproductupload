import re
import os
import requests
from bs4 import BeautifulSoup


class Downloader(object):

    def download_all_images(self, page_body, download_path):
        images_list = []
        # response = requests.get(url)

        # print(response.__dict__)

        # print(response.content)

        soup = BeautifulSoup(page_body, 'html.parser')
        img_tags = soup.find_all('img')

        urls = [img['src'] for img in img_tags]

        # print(len(urls))

        for url in urls:
            print("Downloading: %s" % url)
            filename = re.search(r'/([\w_-]+[.](jpg|gif|png))$', url)
            image_path = os.path.join(download_path, filename.group(1))
            with open(image_path, 'wb') as f:
                if 'http' not in url:
                    # sometimes an image source can be relative
                    # if it is provide the base url which also happens
                    # to be the site variable atm.
                    url = '{}{}'.format(site, url)
                response = requests.get(url)
                f.write(response.content)
            images_list += [image_path]
            print("Downoad done!")
        return images_list
