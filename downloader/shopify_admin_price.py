import time
from browser import Browser


def perform_shopify_login(browser_instance, shop, login, password):
    LOGIN_URL = "https://amusings.myshopify.com/admin/auth/login"
    browser_instance.OpenURL(url=LOGIN_URL)
    login_element = browser_instance.FindElementByName(elementName='login')
    password_element = browser.FindElementByName(elementName='password')

    browser_instance.TypeInto(login, login_element)
    browser_instance.TypeInto(password, password_element)

    browser_instance.HitEnter(password_element)


if __name__ == "__main__":
    print("Started automating prices in shopify admin")
    browser = Browser(headless=False)
    perform_shopify_login(browser, 'amusings', 'joshironak318@gmail.com', 'shopify123')

    print("Logged in...")

    print("Entering the advanced product options app")

    browser.OpenURL('https://amusings.myshopify.com/admin/apps/advanced-product-options')

    login_element = browser.FindElementByName(elementName='login')

    if login_element:
        print("Interruption. Now waiting for manual login")
        browser.wait_until_iframe_loaded('https://apps.mageworx.com/app/productoptions/template')
        browser.OpenURL('https://amusings.myshopify.com/admin/apps/advanced-product-options')

    browser.wait_until_iframe_loaded('https://apps.mageworx.com/app/productoptions/template')

    print("IFrame loaded")

    print("Extracting parameters from URL")

    query_params = browser.get_query_parameters()

    print(query_params)

    base_url = 'https://apps.mageworx.com/app/productoptions/template/edit/29518'

    # new_url = browser.build_url(base_url=base_url, query_params=query_params)

    new_url = base_url + '?hmac=' + query_params['hmac'][0] + '&shop=' + query_params['shop'][0] + '&timestamp=' + query_params['timestamp'][0]

    print('Now entering the app url: %s' % new_url)

    browser.OpenURL(new_url)

    browser.wait_until_iframe_loaded('https://apps.mageworx.com/app/productoptions/template/edit/29518')

    print('App template loaded')

    print("Switch to the iframe")
    browser.switch_to_iframe(frame_class='_2OLP7')

    frames = browser.FindElementByTag("iframe")
    print(frames)

    time.sleep(5)

    print("Now waiting for the search product element to be available")
    browser.wait_until_element_found_by_class('filter-select')

    with open("f.html", "w") as f:
        f.write(str(browser.GetPage()))

    search_product_input = browser.FindElementByClassName('filter-select')

    if search_product_input:
        browser.TypeInto('Despite Collection - S-J-S-1', search_product_input[0])
        print("Product typed")
    else:
        print('Search element not found!')

    time.sleep(3)

    href = None

    tables = browser.FindElementByClassName("items")
    print(tables)
    if tables:
        table = tables[0]
        rows = table.find_elements_by_tag_name("tr")
        print("Rows: %s" % len(rows))
        if rows and len(rows) > 1:
            first_row = rows[1]
            columns = first_row.find_elements_by_tag_name("td")
            first_column = columns[0]
            third_column = columns[2]

            checkbox = first_column.find_elements_by_tag_name("input")
            if checkbox:
                checkbox = checkbox[0]
                browser.ClickElement(checkbox)
                print("Clicked!")
            product_achor = third_column.find_elements_by_tag_name("a")
            if product_achor:
                href = product_achor[0].get_attribute("href")
                print("href: %s" % href)
    else:
        print("Tables not found!")

    print("Switching back")
    browser.switch_back()
    print("Switched back to the original")

    save_template_btn = browser.FindElementByClassName("p_1wLbD")
    if save_template_btn:
        browser.ClickElement(save_template_btn[0])
        print("Template save clicked")
        time.sleep(3)

    if href:
        print("Visiting product URL: %s" % href)
        browser.OpenURL(href)


















