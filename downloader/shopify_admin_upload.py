import os
from openpyxl import load_workbook
import shopify
import time
import requests
from pathlib import Path
import base64
import os
from download_resources import Downloader
from browser import Browser


API_KEY = '86a50e33b5c2b4929cf6782d4c280b40'
PASSWORD = '7ea6474ad63510943814915038c9449b'
SHOP_NAME = 'amusings'
SHARED_SECRET = '062aab1f07c334e4cd581c1c78439912'


def read_excel(file_name):
    wb = load_workbook(file_name, read_only=True)
    sheet = wb.active
    rows = sheet.rows
    first_row = [cell.value for cell in next(rows)]
    data = []
    for i, row in enumerate(rows):
        record = []
        for key, cell in zip(first_row, row):
            if cell.data_type == 's':
                record += [cell.value.strip()]
            else:
                record += [cell.value]
        data += [record]
        if i == 3:
            # print(data)
            # print(len(data))
            pass
    return data[0], data[1:13]


def upload_to_shopify_admin(data):
    shop_url = "https://%s:%s@%s.myshopify.com/admin" % (API_KEY, PASSWORD, SHOP_NAME)
    shopify.ShopifyResource.set_site(shop_url)

    shopify.Session.setup(api_key=API_KEY, secret=SHARED_SECRET)

    session = shopify.Session("%s.myshopify.com" % (SHOP_NAME))

    scope = ["write_products"]
    permission_url = session.create_permission_url(scope)

    params = {
        'client_id': API_KEY,
        'client_secret': SHARED_SECRET,
        'code':  '',
        'timestamp': time.time()
    }

    token = session.request_token(params)
    session = shopify.Session("%s.myshopify.com" % SHOP_NAME, token)
    shopify.ShopifyResource.activate_session(session)


def to_base_64(image_path):
    with open(image_path, 'rb') as file:
        file_content = file.read()

        base64_string = base64.b64encode(file_content).decode('utf8')
        return base64_string


def get_all_products():
    product_schema_url = "https://%s:%s@%s.myshopify.com/admin/products.json" % (API_KEY, PASSWORD, SHOP_NAME)
    print(product_schema_url)
    r = requests.get(product_schema_url)

    return r.json()


def create_product(payload):

    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    create_product_endpoint = "https://%s:%s@%s.myshopify.com/admin/products.json" % (API_KEY, PASSWORD, SHOP_NAME)

    r = requests.post(create_product_endpoint, json=payload,
                      headers=headers)

    return r.json()


def update_product(product_id, updated_payload):

    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    product_upload_url = "https://%s:%s@%s.myshopify.com/admin/products/%s.json" % (API_KEY, PASSWORD, SHOP_NAME, product_id)

    r = requests.put(product_upload_url,json=updated_payload, headers=headers)

    return r.json()


def create_metafield(product_id, meta_field_payload):
    """
    meta_field_payload = {
        "metafield": {
        "namespace": "inventory",
        "key": "warehouse",
        "value": 25,
        "value_type": "integer"
        }
    }"""
    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    create_product_endpoint = "https://%s:%s@%s.myshopify.com//admin/products/%s/metafields.json" % (API_KEY, PASSWORD, SHOP_NAME, product_id)

    r = requests.post(create_product_endpoint, json=meta_field_payload,
                      headers=headers)

    return r.json()


def upload_product_image(product_id, payload):
    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    create_product_endpoint = "https://%s:%s@%s.myshopify.com/admin/products/%s/images.json" % (API_KEY, PASSWORD, SHOP_NAME, product_id)

    r = requests.post(create_product_endpoint, json=payload,
                      headers=headers)

    return r.json()


def upload_single_image(image_path, price_meta_heading, excel_data):
    meta_index_start = 15
    meta_index_end = 24
    price_meta_heading = price_meta_heading[meta_index_start:meta_index_end + 1]

    meta_data = []
    for data_row in excel_data:
        meta_data += [data_row[meta_index_start:meta_index_end + 1]]

    for i, row in enumerate(excel_data):
        print("Processing: %s" % str(i))
        if not row:
            continue
        product_name = row[0]
        tag1 = row[3]
        tag2 = row[4]
        description = row[5]

        # Now create products
        payload = {
            "product": {
                "title": product_name,
                "body_html": description,
                "tags": ", ".join([tag1, tag2])
            }
        }

        response = create_product(payload=payload)

        # print(response)

        product_id = response["product"]["id"]

        b64 = to_base_64(image_path)

        image_name = Path(image_path).name

        payload = {
            "image": {
                "attachment": b64,
                "filename": image_name,
            }
        }

        response = upload_product_image(product_id=product_id, payload=payload)

        # Create meta fields
        for mindex, meta_row in enumerate(meta_data):
            for m in range(meta_index_start, meta_index_end + 1, step=2):
                namespace = price_meta_heading[m]
                key = price_meta_heading[m + 1]
                value = meta_row[m]
                value_type = 'string'

                meta_field_payload = {
                    "metafield": {
                        "namespace": namespace,
                        "key": key,
                        "value": str(value),
                        "value_type": value_type
                    }
                }
                response = create_metafield(product_id=product_id, meta_field_payload=meta_field_payload)


def run_main():
    site_url = 'https://www.pinterest.ca/pin/57772807705661838/'
    file_name = "product_details.xlsx"

    print("Downloading images from: %s" % site_url)

    image_save_directory = os.path.join(os.path.dirname(__file__), 'images')
    if not os.path.exists(image_save_directory):
        os.makedirs(image_save_directory)
    browser = Browser()
    browser.OpenURL(site_url)
    page = browser.GetPage()
    # print(page)
    # return
    downloader = Downloader()
    images_list = downloader.download_all_images(page, image_save_directory)
    print("Image download completed!")

    # images_list = ["/media/codenginebd/MyWorksandStuffs3/Projects/shopifyproductupload/downloader/images/0c728cedfd30a840ba1919bfc2d51b50.jpg"]

    print("Reading the excel file")

    file_path = os.path.join(os.path.dirname(__file__), file_name)
    price_meta_heading, excel_data = read_excel(file_name=file_path)

    print("Product read from excel complete")

    print("Now shopify product create")
    for index, image_path in enumerate(images_list):

        image_name = Path(image_path).name
        print("Working with image %s: %s" % ( index, image_name))

        response = upload_single_image(image_path, price_meta_heading, excel_data)

        print("Finished with image %s: %s" % (index, image_name))

    print("Finished uploading.")

    return
    payload = {
        "product": {
            "title": "Revised Test Product ",
            "body_html": "product for testing body updated",
        }
    }

    response = update_product(product_id="1874244632687", updated_payload=payload)

    print(response)

    # p = get_all_products()
    # print(p)


if __name__ == "__main__":
    run_main()
    file_name = "product_details.xlsx"
    file_path = os.path.join(os.path.dirname(__file__), file_name)
    price_meta_heading, excel_data = read_excel(file_name=file_path)
    print(excel_data)

    product_id = '1874244632687'
    meta_field_payload = {
        "metafield": {
            "namespace": "global",
            "key": "custom_price",
            "value": '7600',
            "value_type": "string"
        }
    }
    response = create_metafield(product_id=product_id, meta_field_payload=meta_field_payload)
    print(response)


