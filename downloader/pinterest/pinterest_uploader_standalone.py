import os
import time
import json
from random import randint
import shutil
from Pinterest import Pinterest


def read_all_images(directory):
    imgs = []
    valid_images = [".jpg", ".gif", ".png", ".jpeg"]
    for f in os.listdir(directory):
        ext = os.path.splitext(f)[1]
        if ext.lower() not in valid_images:
            continue
        imgs += [f]
    return imgs


def move_image(src, dest):
    shutil.move(src, dest)


def start_uploader(image_directory, archive_directory, username, password, product_json_file_path, board_id='599401100344553528'):
    print("Starting upload")
    pinterest = Pinterest(username_or_email=username, password=password)

    logged_in = pinterest.login()

    if logged_in:

        product_json = read_json(product_json_file_path)

        print("Logged In")

        # boards = pinterest.boards()
        # print(boards)
        # return

        print("Reading all images")
        images = read_all_images(image_directory)
        print("Found %s images to process" % len(images))
        i = 1
        for image in images:
            image_path = os.path.join(image_directory, image)
            print("Processing image: %s" % image)
            print("Uploading image")
            image_url = pinterest.upload_image(image_path)
            if image_url:
                print("Image uploaded: %s" % image_url)
            else:
                print("Image upload failed. Skipping...")
                continue
            product_url = product_json.get(image, "")
            print("Product URL: %s" % product_url)
            uploaded_pin = pinterest.pin(
                board_id=board_id,
                image_url=image_url,
                link=product_url
            )
            if uploaded_pin and 'id' in uploaded_pin:
                print("Image %s uploaded successfully. Response: %s" % (image, str(uploaded_pin)))
                print("Moving image to the archive")
                move_image(image_path, archive_directory)
                print("Image %s archived" % image)
            if i % 90 == 0:
                rint = randint(1,5)
                print("Sleeping for %s seconds now" % rint)
                time.sleep(rint)
            i += 1


def read_json(file_path):
    try:
        if os.path.exists(file_path):
            with open(file_path, "r", encoding='utf-8') as rf:
                content = rf.read()
                return json.loads(content)
        else:
            return {}
    except Exception as exp:
        return {}


if __name__ == "__main__":
    username = "joshironak318@gmail.com"
    password = "ronakjoshi12"
    product_json_file_path = os.path.join(os.getcwd(), "products.json")
    image_directory = os.path.join(os.getcwd(), "images")
    image_archived_directory = os.path.join(os.getcwd(), 'images', 'archived')
    if not os.path.exists(image_archived_directory):
        os.makedirs(image_archived_directory)
    start_uploader(image_directory, image_archived_directory, username, password, product_json_file_path)