# - * - coding: UTF-8 - * -

import time
import re
import os
from random import randint
import requests
from bs4 import BeautifulSoup
from urllib import parse
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys


class Browser:

    def __init__(self,headless=False):
        try:
            executable_path = os.path.join(os.getcwd(), 'chromedriver')
            print(executable_path)
            options = webdriver.ChromeOptions()
            if headless:
                options.add_argument('headless')
            options.add_argument('window-size=1200x600')
            self.browser = webdriver.Chrome(executable_path=executable_path, chrome_options=options)
            self.browser.set_page_load_timeout(1000)
            self.browser.set_script_timeout(1000)
        except Exception as exp:
            print(str(exp))
            print("Webdriver open failed.")

    def OpenURL(self,url):
        try:
            self.browser.get(url)
            self.wait = ui.WebDriverWait(self.browser, 60)
            return True
        except Exception as exp:
            return False

    def get_url(self):
        return self.browser.current_url

    def get_query_parameters(self, url=None):
        if not url:
            url = self.get_url()
        query_def = parse.parse_qs(parse.urlparse(url).query)
        return query_def

    def build_url(self, base_url, query_params={}):
        q_params = []
        for k, v in query_params.items():
            q_params += [k+'='+v[0] if v else '']
        return base_url + '?' + ('&'.join(q_params))

    def wait_until_element_found_by_class(self, class_name):
        try:
            WebDriverWait(self.browser, 10).until(EC.presence_of_element_located(
                (By.CLASS_NAME, class_name)))
        except Exception as exp:
            return None

    def GetPage(self):
        try:
            self.page = self.browser.page_source
            encodedStr = self.page.encode("ascii","xmlcharrefreplace")
            return encodedStr
        except Exception as exp:
            return None

    def ClickElement(self,element):
        try:
            if element is not None:
                try:
                    element.click()
                    time.sleep(10)
                    return True
                except Exception as e:
                    print("Click Exception: "+str(e))
                    return False
        except Exception as exp:
            return False

    def HitEnter(self, element):
        try:
            element.send_keys(u'\ue007')
        except Exception as exp:
            print(str(exp))

    def TypeInto(self,text,element):
        try:
            element.send_keys(text)
        except Exception as exp:
            pass

    def FindElementByName(self,elementName):
        try:
            element = self.browser.find_element_by_name(elementName)
            return element
        except Exception as exp:
            return None

    def FindElementById(self,elementId):
        try:
            element = self.browser.find_element_by_id(elementId)
            return element
        except Exception as exp:
            return None

    def FindElementByClassName(self,class_name):
        try:
            element = self.browser.find_elements_by_class_name(class_name)
            return element
        except Exception as exp:
            return None

    def FindElementByText(self,text):
        try:
            element = self.browser.find_element_by_link_text(text)
            return element
        except Exception as exp:
            return None

    def ExecuteScriptAndWait(self,code):
        try:
            self.browser.execute_script(code)
            time.sleep(7)
        except Exception as exp:
            pass

    def GetAllImages(self):
        images = []
        image_elements = self.browser.find_elements_by_tag_name('img')
        for image in image_elements:
            images += [image.get_attribute('src')]
        return images

    def GetPageURL(self):
        try:
            return self.browser.current_url
        except Exception as exp:
            return None

    def ScrollDown(self):
        html = self.browser.find_element_by_tag_name('html')
        html.send_keys(Keys.END)

    def Close(self):
        try:
            self.browser.close()
        except Exception as exp:
            print("Browser closing failed.")


class Downloader(object):

    def download_all_images(self, page_body, download_path):
        images_list = []
        # response = requests.get(url)

        # print(response.__dict__)

        # print(response.content)

        soup = BeautifulSoup(page_body, 'html.parser')
        img_tags = soup.find_all('img')

        urls = [img['src'] for img in img_tags]

        # print(len(urls))

        for url in urls:
            print("Downloading: %s" % url)
            filename = re.search(r'/([\w_-]+[.](jpg|gif|png|jpeg))$', url)
            image_path = os.path.join(download_path, filename.group(1))
            with open(image_path, 'wb') as f:
                if 'http' not in url:
                    # sometimes an image source can be relative
                    # if it is provide the base url which also happens
                    # to be the site variable atm.
                    url = '{}{}'.format('https://www.pinterest.ca', url)
                response = requests.get(url)
                f.write(response.content)
            images_list += [image_path]
            print("Downoad done!")
        return images_list

    def download_all_images_from_urls(self, urls, download_path):
        images_list = []

        for url in urls:
            print("Downloading: %s" % url)
            filename = re.search(r'/([\w_-]+[.](jpg|gif|png|jpeg))$', url)
            image_path = os.path.join(download_path, filename.group(1))
            with open(image_path, 'wb') as f:
                if 'http' not in url:
                    # sometimes an image source can be relative
                    # if it is provide the base url which also happens
                    # to be the site variable atm.
                    url = '{}{}'.format('https://www.pinterest.ca', url)
                response = requests.get(url)
                f.write(response.content)
            images_list += [image_path]
            print("Downoad done!")
        return images_list


def perform_pinterest_login(browser, login, password):
    URL = "https://www.pinterest.ca"
    browser.OpenURL(url=URL)
    login_element = browser.FindElementByName("id")
    password_element = browser.FindElementByName("password")

    browser.TypeInto(login, login_element)
    browser.TypeInto(password, password_element)

    browser.HitEnter(password_element)
    print("Logged In")


if __name__ == '__main__':
    LOGIN = "theworkhaven@gmail.com"
    PASSWORD = "ronakjoshi12"
    NUMBER_OF_SCROLL = 10
    site_urls = ['https://www.pinterest.ca/pin/79798224632477894/',
                 'https://www.pinterest.ca/pin/845762004998177020/',
                 'https://www.pinterest.ca/pin/859765385089773975/',
                 'https://www.pinterest.ca/pin/591660469768424418/',
                 'https://www.pinterest.ca/pin/298785756525353771/',
                 'https://www.pinterest.ca/pin/651262796091230444/',
                 'https://www.pinterest.ca/pin/380906080969204094/',
                 'https://www.pinterest.ca/pin/AY5xQkqf6h9jFt1i6_pWjxkCO-NHLSlYR9KPOWe0I89Gwk0dwx9E408/',
                 'https://www.pinterest.ca/pin/493355334173384419/',
                 'https://www.pinterest.ca/pin/855683997925770623/',
                 'https://www.pinterest.ca/pin/281475045445913964/',
                 'https://www.pinterest.ca/pin/651262796091052353/',
                 'https://www.pinterest.ca/pin/651262796091155201/',
                 'https://www.pinterest.ca/pin/685321268273028307/',
                 'https://www.pinterest.ca/pin/462956036692277420/',
                 'https://www.pinterest.ca/pin/543317142542949216/',
                 'https://www.pinterest.ca/pin/353743745718094462/',
                 'https://www.pinterest.ca/pin/149955862573926746/',
                 'https://www.pinterest.ca/angelogalvez03/mens-outfit-gridsflatlays/']
    image_save_directory = os.path.join(os.getcwd(), 'images')
    if not os.path.exists(image_save_directory):
        os.makedirs(image_save_directory)
    # with open("image_html.html", "r") as f:
    #     page = f.read()
    #     downloader = Downloader()
    #     downloader.download_all_images(page, image_save_directory)
    browser = Browser()
    perform_pinterest_login(browser=browser,
                            login=LOGIN, password=PASSWORD)
    for site_url in site_urls:
        browser.OpenURL(site_url)
        # browser.wait_until_element_found_by_class(class_name="_13 _h _z7 _4q _j")
        # print(page)
        # return
        for i in range(NUMBER_OF_SCROLL):
            rtime = randint(2, 5)
            time.sleep(rtime)
            browser.ScrollDown()
        # page = browser.GetPage()
        image_urls = browser.GetAllImages()
        print(image_urls)
        print(len(image_urls))
        downloader = Downloader()
        downloader.download_all_images_from_urls(image_urls, image_save_directory)
        print("Done for %s!" % site_url)
