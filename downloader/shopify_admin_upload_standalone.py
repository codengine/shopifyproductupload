import os
import json
from random import randint
from openpyxl import load_workbook
import shopify
import time
import requests
from pathlib import Path
import base64
import os
import shutil


API_KEY = '86a50e33b5c2b4929cf6782d4c280b40'
PASSWORD = '7ea6474ad63510943814915038c9449b'
SHOP_NAME = 'amusings'
SHARED_SECRET = '062aab1f07c334e4cd581c1c78439912'


def read_excel(file_name):
    wb = load_workbook(file_name, read_only=True)
    sheet = wb.active
    rows = sheet.rows
    first_row = [cell.value for cell in next(rows)]
    data = []
    for i, row in enumerate(rows):
        record = []
        for key, cell in zip(first_row, row):
            if cell.data_type == 's':
                record += [cell.value.strip()]
            else:
                record += [cell.value]
        data += [record]
    # valid_data = []
    # for row in data:
    #     if not all([not i for i in row]):
    #         valid_data += [row]
    return data[0], data[1:]


def read_excel2(file_name):
    wb = load_workbook(file_name, read_only=True)
    sheet = wb.active
    rows = sheet.rows
    first_row = [cell.value for cell in next(rows)]
    data = []
    for i, row in enumerate(rows):
        record = []
        for key, cell in zip(first_row, row):
            if cell.data_type == 's':
                record += [cell.value.strip()]
            else:
                record += [cell.value]
        data += [record]
        if i == 3:
            # print(data)
            # print(len(data))
            pass
    valid_data = []
    for row in data:
        if not all([not i for i in row]):
            valid_data += [row]
    # print(valid_data)
    return valid_data[1], valid_data[2:]


def upload_to_shopify_admin(data):
    shop_url = "https://%s:%s@%s.myshopify.com/admin" % (API_KEY, PASSWORD, SHOP_NAME)
    shopify.ShopifyResource.set_site(shop_url)

    shopify.Session.setup(api_key=API_KEY, secret=SHARED_SECRET)

    session = shopify.Session("%s.myshopify.com" % (SHOP_NAME))

    scope = ["write_products"]
    permission_url = session.create_permission_url(scope)

    params = {
        'client_id': API_KEY,
        'client_secret': SHARED_SECRET,
        'code':  '',
        'timestamp': time.time()
    }

    token = session.request_token(params)
    session = shopify.Session("%s.myshopify.com" % SHOP_NAME, token)
    shopify.ShopifyResource.activate_session(session)


def to_base_64(image_path):
    with open(image_path, 'rb') as file:
        file_content = file.read()

        base64_string = base64.b64encode(file_content).decode('utf8')
        return base64_string


def get_all_products():
    product_schema_url = "https://%s:%s@%s.myshopify.com/admin/products.json" % (API_KEY, PASSWORD, SHOP_NAME)
    # print(product_schema_url)
    r = requests.get(product_schema_url)

    return r.json()


def get_product(product_id):
    product_schema_url = "https://%s:%s@%s.myshopify.com//admin/products/%s.json" % (API_KEY, PASSWORD, SHOP_NAME, product_id)
    # print(product_schema_url)
    r = requests.get(product_schema_url)

    return r.json()


def create_product(payload):

    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    create_product_endpoint = "https://%s:%s@%s.myshopify.com/admin/products.json" % (API_KEY, PASSWORD, SHOP_NAME)

    session = requests.Session()
    session.trust_env = False
    r = session.post(create_product_endpoint, json=payload,
                      headers=headers)

    return r.json()


def update_product(product_id, updated_payload):

    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    product_upload_url = "https://%s:%s@%s.myshopify.com/admin/products/%s.json" % (API_KEY, PASSWORD, SHOP_NAME, product_id)

    session = requests.Session()
    session.trust_env = False
    r = session.put(product_upload_url,json=updated_payload, headers=headers)

    return r.json()


def create_metafield(product_id, meta_field_payload):
    """
    meta_field_payload = {
        "metafield": {
        "namespace": "inventory",
        "key": "warehouse",
        "value": 25,
        "value_type": "integer"
        }
    }"""
    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    create_product_endpoint = "https://%s:%s@%s.myshopify.com//admin/products/%s/metafields.json" % (API_KEY, PASSWORD, SHOP_NAME, product_id)

    session = requests.Session()
    session.trust_env = False
    r = session.post(create_product_endpoint, json=meta_field_payload,
                      headers=headers)

    return r.json()


def upload_product_image(product_id, payload):
    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    create_product_endpoint = "https://%s:%s@%s.myshopify.com/admin/products/%s/images.json" % (API_KEY, PASSWORD, SHOP_NAME, product_id)

    session = requests.Session()
    session.trust_env = False
    r = session.post(create_product_endpoint, json=payload,
                      headers=headers)

    return r.json()


def generate_all_permutations(list1, list2):
    # list1 = [1,2,3], list2 = [1,2,3]
    permutations = []
    for l1 in list1:
        for l2 in list2:
            permutations += [(l1, l2)]
    return permutations


def add_variants(permutations, prices):
    variants = []
    for perm, price in zip(permutations, prices):
        # print(perm, price)
        variant = {
                'title': str(perm[0]) + " / " + str(perm[1]),
                'option1': str(perm[0]),
                'option2': str(perm[1]),
                'price': float(price)
            }
        variants += [variant]
    return variants


def get_options(option1, option2, option1_values, option2_values):
    options = [
        {
            'name': option1,
            'values': option1_values
        },
        {
            'name': option2,
            'values': option2_values
        }
    ]
    return options


def upload_single_image(image_path, excel_data, jindex, data_row):
    price_meta_heading = excel_data[0]
    excel_data = excel_data[2:]
    product_name_index = 0
    variant1_index = 2
    variant2_index = 3
    variant_price_start = 5
    variant_price_end = variant_price_start + 64

    tag_start = variant_price_end
    tag_end = tag_start + 4

    description_index = tag_end

    meta_field_start = description_index + 1
    meta_field_end = meta_field_start + 10

    meta_data = []
    for data_row in excel_data:
        meta_data += [data_row[meta_field_start:meta_field_end]]

    for i, row in enumerate(excel_data):
        if not row or i != jindex:
            continue

        print("Processing: %s" % str(i))

        product_name = row[product_name_index]

        tags = row[tag_start:tag_end]

        description = row[description_index]

        option1_variants = row[variant1_index].split(',')
        option2_variants = row[variant2_index].split(',')

        permutations = generate_all_permutations(option1_variants, option2_variants)

        # print(product_name)
        # print(tags)
        #
        # print(description)

        # print(len(permutations))

        prices = row[variant_price_start:variant_price_end]

        # print(len(prices))

        variants = add_variants(permutations, prices)

        # Now create products
        payload = {
            "product": {
                "title": product_name,
                "body_html": description,
                "tags": ", ".join(tags),
                "variants": variants,
                "options": get_options(price_meta_heading[variant1_index], price_meta_heading[variant2_index],
                                       row[variant1_index], row[variant2_index])
            }
        }

        response = create_product(payload=payload)

        product_id = response["product"]["id"]

        product_handle = response['product']['handle']

        print(product_id)

        print(product_handle)

        b64 = to_base_64(image_path)

        image_name = Path(image_path).name

        payload = {
            "image": {
                "attachment": b64,
                "filename": image_name,
            }
        }

        response = upload_product_image(product_id=product_id, payload=payload)

        product_data_json = {
            image_name: "https://uptownair.com/products/%s" % product_handle
        }

        save_json(os.path.join(os.getcwd(), "products.json"), product_data_json)

        meta_row = meta_data[i]

        # print(meta_row)

        # break

        # print(row)
        # print(len(meta_row))

        # print(price_meta_heading[meta_field_start:meta_field_end])

        # Create meta fields
        for m in range(meta_field_start, meta_field_end, 2):
            namespace = price_meta_heading[m]
            key = price_meta_heading[m + 1]
            value = row[m + 1]
            value_type = 'string'

            print(namespace)
            print(key)
            print(value)

            meta_field_payload = {
                "metafield": {
                    "namespace": namespace,
                    "key": key,
                    "value": str(value),
                    "value_type": value_type
                }
            }
            response = create_metafield(product_id=product_id, meta_field_payload=meta_field_payload)
            print(response)

        break
    return True


def read_all_images(directory):
    imgs = []
    valid_images = [".jpg", ".gif", ".png", ".jpeg"]
    for f in os.listdir(directory):
        ext = os.path.splitext(f)[1]
        if ext.lower() not in valid_images:
            continue
        imgs += [f]
    return imgs


def move_image(src, dest):
    shutil.move(src, dest)


def read_json(file_path):
    try:
        if os.path.exists(file_path):
            with open(file_path, "r", encoding='utf-8') as rf:
                content = rf.read()
                return json.loads(content)
        else:
            return {}
    except Exception as exp:
        return {}


def save_json(file_path, data_dict):
    current_data = read_json(file_path)
    new_dict = current_data
    for k, v in data_dict.items():
        new_dict[k] = v
    with open(file_path, "w", encoding='utf-8') as tf:
        json.dump(new_dict, tf)


def run_main(excel_file_name):
    print("Reading the excel file")

    file_path = os.path.join(os.getcwd(), excel_file_name)
    price_meta_heading, excel_data = read_excel(file_name=file_path)

    print("Product read from excel complete")

    image_dir = os.path.join(os.getcwd(), 'images')
    print("Reading images from: %s" % image_dir)

    image_archived_directory = os.path.join(os.getcwd(), 'images', 'archived')
    if not os.path.exists(image_archived_directory):
        os.makedirs(image_archived_directory)

    images = read_all_images(image_dir)

    # print(images)

    print("Total %s images found." % len(images))

    img_index = 0

    print(len(excel_data), img_index)

    print("Now shopify product create")
    for index, image_path in enumerate(images):

        try:

            image_name = Path(image_path).name
            print("Working with image %s: %s" % ( index, image_name))

            if len(excel_data) > img_index:
                data_row = excel_data[img_index]
                if any([d for d in data_row]):

                    uploaded = upload_single_image(os.path.join(os.getcwd(), 'images', image_path), excel_data, img_index, data_row)

                    if not uploaded:
                        print("Upload failed. Exiting")
                        return

                    print("Finished with image %s: %s" % (index, image_name))

                    print("Moving image to the archived directory")

                    move_image(os.path.join(os.getcwd(), 'images', image_path), image_archived_directory)

                    print("Image archived")
                else:
                    print("Found empty row")
            else:
                print("No more data")
        except Exception as exp:
            print("Exception occurred. Exception message: %s" % str(exp))
            rtime = randint(15, 20)
            print("Sleeping for %s seconds" % rtime)
            time.sleep(rtime)

        img_index += 1

    print("Finished uploading.")


def test():
    # excel_file_name = "product_details.xlsx"
    # file_path = os.path.join(os.getcwd(), excel_file_name)
    # price_meta_heading, excel_data = read_excel2(file_name=file_path)
    # print(price_meta_heading[:100])
    # # print(len(price_meta_heading[:84]))
    # # print(len(excel_data))
    # # print(excel_data[0])
    # first_data_row = excel_data[0]
    # option1_variants = first_data_row[2].split(',')
    # option2_variants = first_data_row[3].split(',')
    #
    # permutations = generate_all_permutations(option1_variants, option2_variants)
    #
    # # print(len(permutations))
    #
    # prices = first_data_row[5:5+64]
    #
    # variants = add_variants(permutations, prices)

    # print(variants)

    product_id = '1950702895215'

    product = get_product(product_id)
    print(product)


if __name__ == "__main__":
    excel_file_name = "product_details.xlsx"
    run_main(excel_file_name)

    # test()
    # file_path = os.path.join(os.path.dirname(__file__), file_name)
    # price_meta_heading, excel_data = read_excel(file_name=file_path)
    # print(excel_data)
    #
    # product_id = '1874244632687'
    # meta_field_payload = {
    #     "metafield": {
    #         "namespace": "global",
    #         "key": "custom_price",
    #         "value": '7600',
    #         "value_type": "string"
    #     }
    # }
    # response = create_metafield(product_id=product_id, meta_field_payload=meta_field_payload)
    # print(response)


