# - * - coding: UTF-8 - * -

import time
import os
from urllib import parse
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
import selenium.webdriver.support.ui as ui


class Browser:

    def __init__(self,headless=False):
        try:
             options = webdriver.ChromeOptions()
             if headless:
                options.add_argument('headless')
             options.add_argument('window-size=1200x600')
             executable_path = os.path.join(os.path.dirname(__file__), 'chromedriver')
             self.browser = webdriver.Chrome(executable_path=executable_path, chrome_options=options)
             self.browser.set_page_load_timeout(100)
             self.browser.set_script_timeout(100)
        except Exception as exp:
            print(str(exp))
            print("Webdriver open failed.")

    def OpenURL(self,url):
        try:
            self.browser.get(url)
            self.wait = ui.WebDriverWait(self.browser, 60)
            return True
        except Exception as exp:
            return False

    def get_url(self):
        return self.browser.current_url

    def get_query_parameters(self, url=None):
        if not url:
            url = self.get_url()
        query_def = parse.parse_qs(parse.urlparse(url).query)
        return query_def

    def build_url(self, base_url, query_params={}):
        q_params = []
        for k, v in query_params.items():
            q_params += [k+'='+v[0] if v else '']
        return base_url + '?' + ('&'.join(q_params))

    def switch_to_iframe(self, frame_source=None, frame_class=None):
        if frame_source:
            self.browser.switch_to.frame(self.browser.find_element_by_xpath("//iframe[starts-with(@title, '"+ frame_source +"')]"))
        elif frame_class:
            frame_element = self.browser.find_elements_by_class_name(frame_class)
            if frame_element:
                self.browser.switch_to.frame(frame_element[0])
            else:
                print("Not found!")

    def switch_back(self):
        self.browser.switch_to.default_content()

    def wait_until_element_found_by_class(self, class_name):
        try:
            WebDriverWait(self.browser, 1000).until(EC.presence_of_element_located(
                (By.CLASS_NAME, class_name)))
        except Exception as exp:
            return None

    def wait_until_iframe_loaded(self, frame_source, by_class=False):
        if by_class:
            WebDriverWait(self.browser, 10000).until(EC.presence_of_element_located(
                By.CLASS_NAME, "_2OLP7"
            ))
        else:
            WebDriverWait(self.browser, 10000).until(EC.presence_of_element_located(
                (By.XPATH, "//iframe[starts-with(@src, '"+ frame_source +"')]")))

    def GetPage(self):
        try:
            self.page = self.browser.page_source
            encodedStr = self.page.encode("ascii","xmlcharrefreplace")
            return encodedStr
        except Exception as exp:
            return None

    def ClickElement(self,element):
        try:
            if element is not None:
                try:
                    element.click()
                    time.sleep(10)
                    return True
                except Exception as e:
                    print("Click Exception: "+str(e))
                    return False
        except Exception as exp:
            return False

    def HitEnter(self, element):
        try:
            element.send_keys(u'\ue007')
        except Exception as exp:
            print(str(exp))

    def TypeInto(self,text,element):
        try:
            element.send_keys(text)
        except Exception as exp:
            pass

    def ClearText(self,element):
        try:
            element.clear()
        except Exception as exp:
            print('Text Clear Failed!')
            pass

    def FindElementByName(self,elementName):
        try:
            element = self.browser.find_element_by_name(elementName)
            return element
        except Exception as exp:
            return None

    def FindElementById(self,elementId):
        try:
            element = self.browser.find_element_by_id(elementId)
            return element
        except Exception as exp:
            return None

    def FindElementByClassName(self,elementClassName):
        try:
            element = self.browser.find_elements_by_class_name(elementClassName)
            return element
        except Exception as exp:
            return None

    def FindElementByTag(self, tag_name):
        try:
            element = self.browser.find_elements_by_tag_name(tag_name)
            return element
        except Exception as exp:
            return None

    def FindElementByText(self,text):
        try:
            element = self.browser.find_element_by_link_text(text)
            return element
        except Exception as exp:
            return None

    def ExecuteScriptAndWait(self,code):
        try:
            self.browser.execute_script(code)
            time.sleep(7)
        except Exception as exp:
            pass

    def GetPageURL(self):
        try:
            return self.browser.current_url
        except Exception as exp:
            return None

    def Close(self):
        try:
            self.browser.close()
        except Exception as exp:
            print("Browser closing failed.")

    def scroll_to_pager_link(self):
        try:
            scrollto_pager_link = 'var el = document.getElementById("Y-N-ffs");el.scrollIntoView(true);'
            #scrollto_pager_link = 'var el = document.getElementsByClassName("pagination");el[0].scrollIntoView(true);'
            self.ExecuteScriptAndWait(scrollto_pager_link)
        except Exception as exp:
            print("Exception Inside page scroll. %s" % str(exp))






