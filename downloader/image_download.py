import os
from download_resources import Downloader
from browser import Browser


if __name__ == '__main__':
    site_url = 'https://www.pinterest.ca/pin/57772807705661838/'
    image_save_directory = os.path.join(os.path.dirname(__file__), 'images')
    if not os.path.exists(image_save_directory):
        os.makedirs(image_save_directory)
    browser = Browser()
    browser.OpenURL(site_url)
    page = browser.GetPage()
    # print(page)
    # return
    downloader = Downloader()
    downloader.download_all_images(page, image_save_directory)
    print("Done!")